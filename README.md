### Previo integration project for the hotel

Application is written in spring boot (open-jdk 11) and react-js. Postgres db is used to store its data. Another db is used to read POS data.

Purpose of the application is the integration between previo reservation system (http://apimanual.previo.cz/en/manual) and septim Point Of Sale database.
Application also provides basic frontend for management of payments and reservations.
To access the application the user must be connected via vpn.

### To run in idea locally:
1. Add framework support for maven  ``` https://www.jetbrains.com/help/idea/convert-a-regular-project-into-a-maven-project.html ```
2. Create file /src/main/resources/application.properties and fill with configuration, never commit on git because it contains sensitive information (check properties on server)
3. Find class Application.java and run it
4. For connection to septim db there must be tunnel for dev purposes.

### build frontend: 
use npm version 16, there is a bug in later release for the create-react-app scripts
1. npm run build
2. copy '\frontend\build' to '\src\main\resources\static'

### build backend and run: 
1. maven clean package
2. copy jar to /srv/app/integration as 'integration.jar' on server
3. application runs on linux as a service, restart: ```systemctl restart previo_integration.service```

### Swagger: ``` http://localhost:8089/swagger-ui.html ``` (8089 depends on port you run, default is 8080).

### start and stop
cp /home/asw/integration.jar /srv/app/integration/
systemctl restart previo_integration.service
systemctl status previo_integration.service
systemctl stop previo_integration.service

