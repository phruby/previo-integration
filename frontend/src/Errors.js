import React from 'react';

const Errors = ({error}) => {

    function confirm () {
        window.location.reload();
    }
    if (!error) {
        return null;
    }
    return <div className='errorMessage'>
        <div className='errorMessage'>ErrorMessage: {error}</div>
        <button onClick={confirm} type="button" className="btn btn-warning">schovat error, zkusim dál pracovat</button>
    </div>


};
export default Errors;