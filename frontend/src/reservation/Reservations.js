import React, {useEffect, useState} from 'react';
import axios from "axios";
import ReservationRow from "./ReservationRow";
import PaymentsConnectedToReservation from "./PaymentsConnectedToReservation";
import Errors from "../Errors";

const Reservations = () => {

    const [reservations, setReservations] = useState([]);

    const [error, setError] = useState(null);

    // filters
    const [states, setStates] = useState(['new']);
    const [daysAgo, setDaysAgo] = useState(10);

    // selected values
    const [voucher, setVoucher] = useState(null);
    const [selectedReservation, setSelectedReservation] = useState(null);

    useEffect(async () => {
        await reloadReservations();
    }, []);

    const reloadReservations = async () => {
        const response = await axios.post('/backend/reservations-get', {states, daysAgo})
            .catch(error => {
                const {status} = error.response || 'bohužel backend nezaslal ani status';
                const {data} = error.response || {};
                const {message} = data || 'bohužel backend nezaslal error zprávu';
                setError('status: ' + status + ', message: error během načítání rezervací  - ' + message);
            });
        if (response) {
            setReservations(response.data.reservations);
        }
    };

    const changeReservationState = async (reservationId, voucher, newState) => {
        const response = await axios.post('/backend/reservation-update', {resId: reservationId, voucher: voucher, state: newState}).catch(error => {
            const {status} = error.response || 'bohužel backend nezaslal ani status';
            const {data} = error.response || {};
            const {message} = data || 'bohužel backend nezaslal error zprávu';
            setError('status: ' + status + ', message: error během modifikace rezervace ' + message);
        });
        if (response) {
            const newReservations = reservations.map((reservation) => {
                if (reservation.resId === reservationId && reservation.voucher === voucher) {
                    return {
                        ...reservation,
                        state: newState,
                    };
                }

                return reservation;
            });
            setReservations(newReservations);
        }
    };

    async function setFilterStates(state) {
        if (states.includes(state)) {
            const newList = states.filter((item) => item !== state);
            setStates(newList);
        } else {
            const newList = states.concat(state);
            setStates(newList);
        }
    }

    return <div className='reservation'>
        {/*filters*/}
        <div className='header'>Rezervace</div>
        <div className="row">
            <div className="form-check col-lg-3 col-md-4 col-sm-6">
                <div className="filters">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" checked={states.includes('new')} onChange={() => setFilterStates('new')}/>
                    <label className="form-check-label" htmlFor="exampleCheck1" title="Check me out">Ke schválení</label>
                </div>
                <div className="filters">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" checked={states.includes('ok')} onChange={() => setFilterStates('ok')}/>
                    <label className="form-check-label" htmlFor="exampleCheck1" title="Check me out">Schválené</label>
                </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-12">
                <span>Dní nazpět: </span><input type="daysAgo" id="daysAgo" onChange={(e) => setDaysAgo(e.target.value)} value={daysAgo}/>
                <button onClick={() => reloadReservations()}>Potvrdit</button>
            </div>
        </div>

        {/* errors*/}
        <Errors error={error}/>

        <div className='flex'>

            {/* list of reservations */}
            <div className='left container'>
                <table className="table">
                    <thead className="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Voucher</th>
                        <th scope="col">Od</th>
                        <th scope="col">Do</th>
                        <th scope="col">Cena</th>
                        <th scope="col">Saldo</th>
                        <th scope="col">Zaplaceno</th>
                        <th scope="col">Kurz</th>
                        <th scope="col">Stav</th>
                        <th className="col-width-buttons" scope="col"/>
                    </tr>
                    </thead>
                    <tbody>
                    {reservations.map((reservation, index) => {
                        return <ReservationRow
                            reservation={reservation}
                            setVoucher={setVoucher}
                            setSelectedReservation={setSelectedReservation}
                            selectedVoucher={voucher}
                            changeReservationState={changeReservationState}
                            key={'reservation' + '|' + index}
                        />
                    })}
                    </tbody>
                </table>
            </div>

            {/* payment info */}
            <div className='right container'>
                <PaymentsConnectedToReservation
                    selectedVoucher={voucher}
                    reloadReservations={reloadReservations}
                    reservation={selectedReservation}
                />
            </div>
        </div>
    </div>


};


export default Reservations;