import React from 'react';

const PaymentsConnectedToReservationRow = ({payment, unPairPayment}) => {

    function unPair() {
        unPairPayment(payment.id);
    }

    return <tr>
        <td>{payment.id}</td>
        <td>{payment.dateTime}</td>
        <td>{payment.price}</td>
        <td>{payment.cashRegister}</td>
        <td>{payment.receipt}</td>
        <button onClick={unPair} type="button" className="btn btn-danger">odmazat</button>
    </tr>
};


export default PaymentsConnectedToReservationRow;