import React, {useEffect, useState} from 'react';
import axios from "axios";
import PaymentsConnectedToReservationRow from "./PaymentsConnectedToReservationRow";
import Errors from "../Errors";

const PaymentsConnectedToReservation = ({selectedVoucher, reloadReservations, reservation}) => {

    console.log('reservation');
    console.log(reservation);

    const [payments, setPayments] = useState([]);
    const [error, setError] = useState(null);

    const {note} = reservation | {};
    const [newNote, setNewNote] = useState(note);

    useEffect(async () => {
        if (selectedVoucher) {
            await reloadPayments();
        }
    }, [selectedVoucher]);

    useEffect(async () => {
        if (reservation && reservation.note) {
            setNewNote(reservation.note);
        } else {
            setNewNote('');
        }
    }, [reservation]);

    if (!selectedVoucher) {
        return null;
    }

    const changeNoteValue = (e) => {
        setNewNote(e.target.value);
    };

    const saveNote = async () => {
        const response = await axios.post('/backend/reservation-update', {resId: reservation.resId, note: newNote, voucher: selectedVoucher})
            .then(() => reloadReservations())
            .catch(error => {
            const {status} = error.response || 'bohužel backend nezaslal ani status';
            const {data} = error.response || {};
            const {message} = data || 'bohužel backend nezaslal error zprávu';
            setError('status: ' + status + ', message: error během modifikace poznamky ' + message);
        });
    };
    const unPairPayment = async (id) => {

        const response = await axios.post('/backend/un-pair-payment', {id}).catch(error => {
            const {status} = error.response || 'bohužel backend nezaslal ani status';
            const {data} = error.response || {};
            const {message} = data || 'bohužel backend nezaslal error zprávu';
            setError('status: ' + status + ', message: error během smazání platby  - ' + message);
        });
        if (response) {
            await reloadPayments();
            await reloadReservations();
        }
    };

    const reloadPayments = async () => {
        const response = await axios.post('/backend/get-payments-by-voucher', {voucher: selectedVoucher}).catch(error => {
            const {status} = error.response || 'bohužel backend nezaslal ani status';
            const {data} = error.response || {};
            const {message} = data || 'bohužel backend nezaslal error zprávu';
            setError('status: ' + status + ', message: error během načítání plateb přiřazeným k rezervaci  - ' + message);
        });
        if (response) {
            setPayments(response.data.payments);
        }
    };

    return <div>
        {/* Payment info */}
        <h2>Voucher: {selectedVoucher}</h2>
        <Errors error={error}/>
        <table className="table">
            <thead className="thead-dark">
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Cas</th>
                <th scope="col">Castka</th>
                <th scope="col">pokladna</th>
                <th scope="col">listek</th>
                <th className="col-width-buttons" scope="col"/>
            </tr>
            </thead>
            <tbody>
            {payments.map(payment => {
                return <PaymentsConnectedToReservationRow payment={payment} unPairPayment={unPairPayment} key={payment.id}/>
            })}
            </tbody>
        </table>

        {/* Note info */}
        Poznámka:
        <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={newNote} onChange={changeNoteValue}/>
        <button style={{marginTop: 10}} onClick={saveNote} type="button" className="btn btn-success">Uložit</button>
        <div style={{marginTop: 20}}>Na jméno: </div>
        <div style={{fontSize: 25, fontWeight: 'bold'}}>{reservation.contactPerson}</div>
    </div>


};


export default PaymentsConnectedToReservation;