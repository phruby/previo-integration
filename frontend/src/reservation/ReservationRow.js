import React from 'react';

const ReservationRow = ({reservation,  changeReservationState, setVoucher, selectedVoucher, setSelectedReservation}) => {

    const {voucher, from, to, paid, price, currency, state, resId, paidDeposit} = reservation;

    const saldo = (price * 100 - paidDeposit * 100) / 100;

    function calcExchangeRate(saldo, paid, currency) {
        if (currency.toLowerCase() === 'czk' || !paid || !saldo) {
            return null;
        }
        return (paid / saldo).toFixed(2);
    }

    const exchangeRate = calcExchangeRate(saldo, paid, currency);

    const confirm = (e) => changeReservationState(resId, voucher, 'ok');
    const reset = (e) => changeReservationState(resId, voucher, 'new');
    const displayDetail = (e) => {
        setVoucher(voucher);
        setSelectedReservation(reservation);
    };

    const chosen = (selectedVoucher === voucher);

    return <tr onClick={displayDetail} className={chosen ? 'chosen' : null}>
        <td>{resId}</td>
        <td>{voucher}</td>
        <td>{from}</td>
        <td>{to}</td>
        <td>{price} {currency}</td>
        <td>{saldo}</td>
        <td>{paid}</td>
        <td>{exchangeRate}</td>
        {/*  TODO count not working on backend correctly <td>{count > 1 && count}</td>*/}
        <td>{state}</td>
        <td>
            {state === 'new' && <button onClick={confirm} type="button" className="btn btn-success">Schválit</button>}
            {state === 'ok' && <button onClick={reset} type="button" className="btn btn-warning">Vrátit</button>}
        </td>
    </tr>


};


export default ReservationRow;