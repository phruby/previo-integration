import './App.css';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Payments from "./payment/Payments";
import Reservations from "./reservation/Reservations";
import Synchonizations from "./sync/Synchonizations";

function App() {
    return (
        <Router>
            <div>
                <header>
                    <nav>
                        <ul>
                            <li>
                                <Link to="/reservations">Rezervace</Link>
                            </li>
                            <li>
                                <Link to="/payments">Platby</Link>
                            </li>
                            <li>
                                <Link to="/sync">Synchronizace</Link>
                            </li>
                        </ul>
                    </nav>
                </header>

                {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/reservations">
                        <Reservations/>
                    </Route>
                    <Route path="/payments">
                        <Payments/>
                    </Route>
                    <Route path="/sync">
                        <Synchonizations/>
                    </Route>
                    <Route path="/">
                        <Reservations/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
