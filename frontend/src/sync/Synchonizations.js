import Errors from "../Errors";
import {useState, useEffect} from "react";
import axios from "axios";
import SynchronizationRow from "./SynchronizationRow";

const Synchonizations = () => {
    const [synchonizations, setSynchonizations] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(async() => {
            await reloadSynchronizations();
    }, []);

    const reloadSynchronizations = async () => {
        const response = await axios.post('/backend/get-last-synchronization', {})
            .catch(error => {
                const {status} = error.response || 'bohužel backend nezaslal ani status';
                const {data} = error.response || {};
                const {message} = data || 'bohužel backend nezaslal error zprávu';
                setError('status: ' + status + ', message: error během načítání synchronizaci  - ' + message);
            });
        if (response) {
            setSynchonizations(response.data.synchronizations);
        }
    };

    const syncData = async () => {
        if (!loading) {
            setLoading(true);
            const response = await axios.post('/backend/sync-data', {})
                .catch(error => {
                    const {status} = error.response || 'bohužel backend nezaslal ani status';
                    const {data} = error.response || {};
                    const {message} = data || 'bohužel backend nezaslal error zprávu';
                    setError('status: ' + status + ', message: error během synchronizace dat - ' + message);
                });
            if (response) {
                setLoading(false);
                await reloadSynchronizations();
            }
        }
    };

    return <div className='sync'>
        <div className='header'>Poslední synchronizace</div>
        <div className='flex'>
            <div className='left container'>
                <Errors error={error}/>
                <div className="padding-15">
                    <div>{loading && 'synchronizace probíhá'}</div>
                    <button onClick={syncData} type="button" className="btn btn-warning">Synchronizovat teď</button>
                </div>
                <table className="table">
                    <thead className="thead-dark">
                    <tr>
                        <th scope="col">Název</th>
                        <th scope="col">Start</th>
                        <th scope="col">Konec</th>
                        <th scope="col">Status</th>
                        <th scope="col">Error</th>
                    </tr>
                    </thead>
                    <tbody>
                    {synchonizations.map(sync => {
                        return <SynchronizationRow sync={sync} key={sync.id} />
                    })}
                    </tbody>
                </table>
            </div>
        </div>
    </div>


};
export default Synchonizations;