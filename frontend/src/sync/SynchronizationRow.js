import React, {useState} from 'react';
import axios from "axios";

const SynchronizationRow = ({sync}) => {

    const {name, finishedAt, createdAt, status, error} = sync;


    return <tr>
        <td>{name}</td>
        <td>{createdAt}</td>
        <td>{finishedAt}</td>
        <td>{status}</td>
        <td>{error}</td>
    </tr>


};


export default SynchronizationRow;