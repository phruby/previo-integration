import React, {useState} from 'react';
import axios from "axios";

const PaymentRow = ({payment, setError, reloadData}) => {

    const {id, dateTime, receipt, price, cashRegister, voucher} = payment;

    const [voucherInput, setVoucherInput] = useState(voucher);

    async function editVoucher() {
        const response = await axios.post('/backend/pair-payment', {id, voucher: voucherInput})
            .catch(error => {
                const {status} = error.response || 'bohužel backend nezaslal ani status';
                const {data} = error.response || {};
                const {message} = data || 'bohužel backend nezaslal error zprávu';
                setError('status: ' + status + ', message: error během načítání nespárovaných plateb  - ' + message);
            });
        if (response) {
            await reloadData()
        }
    }

    function handleChange(event) {
        setVoucherInput(event.target.value);
    }

    return <tr>
        <td>{id}</td>
        <td>{dateTime}</td>
        <td>{receipt}</td>
        <td>{price}</td>
        <td>{cashRegister}</td>
        <td><input onChange={handleChange} value={voucherInput} type="text" /></td>
        <button onClick={editVoucher} type="button" className="btn btn-success">Uložit</button>
    </tr>


};


export default PaymentRow;