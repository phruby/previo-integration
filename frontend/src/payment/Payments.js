import Errors from "../Errors";
import React, {useState, useEffect} from "react";
import PaymentRow from "./PaymentRow";
import axios from "axios";

const Payments = () => {
    const [payments, setPayments] = useState([]);
    const [error, setError] = useState(null);
    const [daysAgo, setDaysAgo] = useState(10);

    useEffect(async () => {
        await reloadPayments();
    }, []);

    const reloadPayments = async () => {
        const response = await axios.post('/backend/get-un-paired-payments', {daysAgo})
            .catch(error => {
                const {status} = error.response || 'bohužel backend nezaslal ani status';
                const {data} = error.response || {};
                const {message} = data || 'bohužel backend nezaslal error zprávu';
                setError('status: ' + status + ', message: error během načítání nespárovaných plateb  - ' + message);
            });
        if (response) {
            setPayments(response.data.payments);
        }
    };

    return <div className='payment'>
        <div className='header'>Nespárované platby</div>
        <Errors error={error}/>
        <div className='flex'>
            <div className='left container'>
                <div>
                    <span>Dní nazpět: </span><input type="daysAgo" id="daysAgo" onChange={(e) => setDaysAgo(e.target.value)} value={daysAgo}/>
                    <button onClick={() => reloadPayments()}>Potvrdit</button>
                </div>
                <table className="table">
                    <thead className="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Datum</th>
                        <th scope="col">Účtenka</th>
                        <th scope="col">Cena</th>
                        <th scope="col">Pokladna</th>
                        <th scope="col">Voucher</th>
                        <th scope="col"/>
                    </tr>
                    </thead>
                    <tbody>
                    {payments.map(payment => {
                        return <PaymentRow payment={payment} setError={setError} reloadData={reloadPayments} key={payment.id}/>
                    })}
                    </tbody>
                </table>
            </div>
            {/*<div className='right container'><PaymentsConnectedToReservation selectedVoucher={voucher} reloadReservations={reloadPayments}/></div>*/}
        </div>
    </div>


};
export default Payments;