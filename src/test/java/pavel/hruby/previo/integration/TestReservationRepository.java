package pavel.hruby.previo.integration;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pavel.hruby.previo.integration.repository.DbTransaction;
import pavel.hruby.previo.integration.repository.reservation.ReservationDao;
import pavel.hruby.previo.integration.repository.reservation.ReservationFilter;
import pavel.hruby.previo.integration.repository.reservation.ReservationRepository;
import pavel.hruby.previo.integration.repository.reservation.ReservationState;
import pavel.hruby.previo.integration.repository.reservation.UpdateReservation;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TestReservationRepository {

    @Autowired
    private ReservationRepository repo;

    @Autowired
    DbTransaction dbTransaction;

    @Test
    public void integrationTest () throws Exception {

        // run in transaction dont commit (cause sometimes might test fail and leave data in db)
        try (DbTransaction.Status status = dbTransaction.openNewTransaction()) {

            // store
            ReservationDao insertedDao = newReservation();
            repo.store(insertedDao);
            String comId = insertedDao.getComId();

            // check in the db for inserted row
            ReservationFilter filter = ReservationFilter.builder().comIds(Arrays.asList(comId, "nonExistingId")).build();
            List<ReservationDao> list = repo.getReservations(filter);
            Assert.assertEquals(1, list.size());
            Assert.assertEquals(insertedDao, list.get(0));
            Assert.assertTrue(insertedDao.allFieldsEquals(list.get(0)));

            repo.getReservationsForListing(filter);
            Assert.assertEquals(1, list.size());

            // update
            BigDecimal newPrice = BigDecimal.TEN;
            String newVoucher =  "newVoucher";
            Instant newFromDate = Instant.now().plusMillis(10).truncatedTo(ChronoUnit.MICROS);
            Instant newToDate = Instant.now().plusMillis(10).truncatedTo(ChronoUnit.MICROS);
            Instant newUpdatedAt = Instant.now().plusMillis(10).truncatedTo(ChronoUnit.MICROS);
            ReservationState newState =  ReservationState.OK;

            UpdateReservation update = UpdateReservation.builder()
                    .comId(comId)
                    .price(newPrice)
                    .voucher(newVoucher)
                    .fromDate(newFromDate)
                    .toDate(newToDate)
                    .updateAt(newUpdatedAt)
                    .state(newState)
                    .build();

            repo.update(update);

            // check if updated correctly
            list = repo.getReservations(filter);
            Assert.assertEquals(1, list.size());
            Assert.assertEquals(insertedDao, list.get(0));
            Assert.assertFalse(insertedDao.allFieldsEquals(list.get(0)));
            Assert.assertEquals(newPrice, list.get(0).getPrice());
            Assert.assertEquals(newVoucher, list.get(0).getVoucher());
            Assert.assertEquals(newFromDate, list.get(0).getFrom());
            Assert.assertEquals(newToDate, list.get(0).getTo());
            Assert.assertEquals(newUpdatedAt, list.get(0).getUpdatedAt());
            Assert.assertEquals(newState, list.get(0).getState());

            // delete
            repo.delete(comId);
            list = repo.getReservations(filter);
            Assert.assertEquals(0, list.size());
        }
    }



    private ReservationDao newReservation() {
        return new ReservationDao(
                TestUtil.random(7),
                "resId",
                "voucher",
                Instant.now().truncatedTo(ChronoUnit.MICROS),
                Instant.now().truncatedTo(ChronoUnit.MICROS),
                Instant.now().truncatedTo(ChronoUnit.MICROS),
                ReservationState.NEW,
                BigDecimal.ONE,
                "currency",
                BigDecimal.ZERO,
                false,
                null,
                "contactPerson"
        );
    }
}
