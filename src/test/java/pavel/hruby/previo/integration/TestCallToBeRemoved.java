package pavel.hruby.previo.integration;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pavel.hruby.previo.integration.http.previo.rest.RestPrevioHttpService;
import pavel.hruby.previo.integration.http.previo.xml.XmlPrevioHttpService;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioPaymentResponseItem;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCallToBeRemoved {

    @Autowired
    private XmlPrevioHttpService previoHttpService;

    @Autowired
    private RestPrevioHttpService restPrevioHttpService;

    @Test
    public void testCall () throws Exception {

        List<PrevioPaymentResponseItem> response = previoHttpService.getPayments("56405175");
        System.out.println();

    }

    @Test
    public void restTestCall() {
        restPrevioHttpService.getPayments(null);
    }
}
