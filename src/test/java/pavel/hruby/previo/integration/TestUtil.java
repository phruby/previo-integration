package pavel.hruby.previo.integration;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class TestUtil {

    public static String random(int length) {
        byte[] array = new byte[length]; // length is bounded by 7
        new Random().nextBytes(array);
        return new String(array, StandardCharsets.UTF_8);
    }
}
