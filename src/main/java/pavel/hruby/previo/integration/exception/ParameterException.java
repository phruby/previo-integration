package pavel.hruby.previo.integration.exception;

public class ParameterException extends RuntimeException {

    public ParameterException(String message) {
        super(message);
    }
}
