package pavel.hruby.previo.integration.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pavel.hruby.previo.integration.api.ApiError;
import pavel.hruby.previo.integration.api.ApiErrorCode;

import java.util.Arrays;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger log = LogManager.getLogger(ResponseExceptionHandler.class.getName());

    /**
     * Handler for REST validation errors
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request){
        return new ResponseEntity<>(new ApiError(ex.getMessage(), ApiErrorCode.INVALID_REQUEST_BODY), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle exception thrown by own java code
     */
	@ExceptionHandler(Throwable.class)
	public final ResponseEntity<ApiError> handleGeneralException(Throwable ex) {

	    log.error("invalid request", ex);
        if (ex instanceof ParameterException) {
            return new ResponseEntity<>(new ApiError(ex.getMessage(), null), HttpStatus.BAD_REQUEST);
        }
        if (ex instanceof NoRecordUpdated) {
            return new ResponseEntity<>(new ApiError("no record found in db", ApiErrorCode.NO_RECORD), HttpStatus.BAD_REQUEST);
        }
        if (ex instanceof Exception) {
            return new ResponseEntity<>(new ApiError(ex.getMessage() + Arrays.toString(ex.getStackTrace()), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        throw new RuntimeException("should not reach here.", ex);
	}

}
