package pavel.hruby.previo.integration.repository.reservation;

import pavel.hruby.previo.integration.exception.ParameterException;
import pavel.hruby.previo.integration.interfejs.Enumerable;

public enum ReservationState implements Enumerable {

    NEW("new"),
    OK("ok");

    private final String dbValue;

    ReservationState(String value) {
        this.dbValue = value;
    }

    public static ReservationState fromDb(String value) {
        for (ReservationState type: ReservationState.values() ) {
            if (type.dbValue.equals(value)) {
                return type;
            }
        }
        return null;
    }
    public static ReservationState fromApi(String value) {
        for (ReservationState type: ReservationState.values() ) {
            if (type.dbValue.equals(value)) {
                return type;
            }
        }
        throw new ParameterException("wrong value: " + value);
    }

    public static ReservationState fromApiOptional(String fieldName, String value) {
        if (value == null)
            return null;

        try {
            return fromApi(value);
        } catch (ParameterException ex) {
            throw new ParameterException(fieldName + ": " + ex.getMessage());
        }
    }


    public String getDbValue() {
        return dbValue;
    }
}

