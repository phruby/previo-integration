package pavel.hruby.previo.integration.repository.septim;


import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.util.JdbcUtil;

import java.util.List;

@Service
@AllArgsConstructor
public class SeptimRepository {

    private final SeptimDatabase septimDatabase;

    public List<SeptimPayment> getPayments() {

        NamedParameterJdbcTemplate jdbcTemplate = septimDatabase.getJdbcTemplate();
        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = "select _id, popis, cenas, datumcas, recept, pokladna, _smazan" +
                " from admin_septim.uzavucet_uupol_v " +
                "where skuprec1 ='Hotel' " +
                "and recept like 'OBZ_UBYT%' ";

        return jdbcTemplate.query(sql, params, (rs, i) -> new SeptimPayment(
                rs.getLong("_id"),
                rs.getString("popis"),
                rs.getBigDecimal("cenas"),
                JdbcUtil.getInstant("datumcas", rs),
                rs.getString("recept"),
                rs.getString("pokladna"),
                rs.getInt("_smazan")
        ));
    }
}
