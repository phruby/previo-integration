package pavel.hruby.previo.integration.repository.septim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.config.SeptimDatabaseConfig;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;


@Service
public class SeptimDatabase {

    @Autowired
    private SeptimDatabaseConfig septimDatabaseConfig;

    private static final Logger logger = LogManager.getLogger(SeptimDatabase.class);

    private NamedParameterJdbcTemplate jdbcTemplate = null;

    @PostConstruct
    private void configSeptimDb() {
        //do we have secondary database configuration?
        if(septimDatabaseConfig.isEnabled()) {

            DataSource secondaryDataSource = DataSourceBuilder.create()
                    .url(septimDatabaseConfig.getUrl())
                    .username(septimDatabaseConfig.getUsername())
                    .password(septimDatabaseConfig.getPassword())
                    .build();

            this.jdbcTemplate = new NamedParameterJdbcTemplate(secondaryDataSource);
            logger.info("Septim database is configured");

            //test connection
            try {
                jdbcTemplate.getJdbcOperations().execute("SELECT 1");
                logger.info("Septim database is running");
            } catch (Exception e) {
                logger.error("Cannot connect to the Septim database " + e.getMessage(),e);
                throw new RuntimeException("Cannot connect to the Septim database " + e.getMessage(), e);
            }
        }
    }

    NamedParameterJdbcTemplate getJdbcTemplate() {
        if(!septimDatabaseConfig.isEnabled()) {
            throw new RuntimeException("septim is not configured. That usually happen because of dev purposes");
        }
        prepareSession();
        sessionLogin();
        return jdbcTemplate;
    }

    private void prepareSession() {
        String sql = "select admin_core.sessionprepare()";
        jdbcTemplate.query(sql, new MapSqlParameterSource(), resultSet -> null);
    }

    private void sessionLogin() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("aswUser", septimDatabaseConfig.getAswUser());
        params.addValue("aswPwd", septimDatabaseConfig.getAswPassword());

        String sql = "select admin_core.SESSIONLogin(:aswUser, :aswPwd, 'skript', ' ', true)";
        jdbcTemplate.query(sql, params, resultSet -> null);
    }


}
