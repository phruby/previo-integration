package pavel.hruby.previo.integration.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class improves usage of PlatformTransactionManager.
 * It has better interface and allows to check, if the current thread is in transaction.
 * If thread is still in transaction while returning data to caller, the close method has not been called properly.
 */
@Service
public class DbTransaction {

    @Autowired
    private PlatformTransactionManager transactionManager;

    //each thread has counter of nested transactions
    private static ThreadLocal<AtomicInteger> transactionCheck = ThreadLocal.withInitial(()->new AtomicInteger());

    public class Status implements AutoCloseable {

        private TransactionStatus transactionStatus;

        private Status(TransactionStatus transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        /**
         * Transaction commit
         */
        public void commitTransaction() {
            transactionManager.commit(this.transactionStatus);
        }

        /**
         * Always call this method in final block
         */
        @Override
        public void close() {
            transactionCheck.get().decrementAndGet();
            if(!this.transactionStatus.isCompleted()) {
                //commit was not called, so rollback it now
                transactionManager.rollback(this.transactionStatus);
            }
        }
    }

    /**
     * Starts a new database transaction. Always call close on returned status.
     * @return
     */
    public Status openNewTransaction() {
        Status status = new Status(transactionManager.getTransaction(new DefaultTransactionDefinition()));
        transactionCheck.get().incrementAndGet();
        return status;
    }

    /**
     * Returns true if current thread is in database transaction
     * @return
     */
    public static boolean isInTransaction() {
        return transactionCheck.get().intValue() != 0;
    }
}

