package pavel.hruby.previo.integration.repository.reservation;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.util.JdbcUtil;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;


@Service
@AllArgsConstructor
public class ReservationRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String COM_ID = "com_id";
    private static final String RES_ID = "res_id";
    private static final String VOUCHER = "voucher";
    private static final String FROM_DATE = "from_date";
    private static final String TO_DATE = "to_date";
    private static final String UPDATED_AT = "updated_at";
    private static final String STATE = "state";
    private static final String PRICE = "price";
    private static final String CURRENCY = "currency";
    private static final String PAID_DEPOSIT = "paid_deposit";
    private static final String NOTE = "note";
    private static final String CONTACT_PERSON = "contact_person";

    public void store(ReservationDao reservation) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COM_ID, JdbcUtil.toDb(reservation.getComId()));
        params.addValue(RES_ID, JdbcUtil.toDb(reservation.getResId()));
        params.addValue(VOUCHER, JdbcUtil.toDb(reservation.getVoucher()));
        params.addValue(FROM_DATE, JdbcUtil.toDb(reservation.getFrom()));
        params.addValue(TO_DATE, JdbcUtil.toDb(reservation.getTo()));
        params.addValue(UPDATED_AT, JdbcUtil.toDb(reservation.getUpdatedAt()));
        params.addValue(STATE, JdbcUtil.toDb(ReservationState.NEW));
        params.addValue(PRICE, JdbcUtil.toDb(reservation.getPrice()));
        params.addValue(CURRENCY, JdbcUtil.toDb(reservation.getCurrency()));
        params.addValue(PAID_DEPOSIT, JdbcUtil.toDb(reservation.getPaidDeposit()));
        params.addValue(CONTACT_PERSON, JdbcUtil.toDb(reservation.getContactPerson()));

        String sql = "insert into integration.reservation(com_id, res_id, voucher, from_date, to_date, updated_at, state, price, currency, paid_deposit, contact_person) VALUES (:com_id, :res_id, :voucher, :from_date, :to_date, :updated_at, :state, :price, :currency, :paid_deposit, :contact_person)";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
    }

    public List<ReservationDao> getReservations(@NotNull ReservationFilter filter) {

        if (filter == null)
            throw new RuntimeException("filter cannot be null");

        if (filter.getComIds() != null && filter.getComIds().isEmpty())
            throw new RuntimeException("no reservations asking?");

        if (filter.getStates() != null && filter.getStates().isEmpty())
            return Collections.emptyList();

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("comIds", JdbcUtil.toDb(filter.getComIds()));
        params.addValue("fromDate", JdbcUtil.toDb(filter.getFromDate()));
        params.addValue("toDate", JdbcUtil.toDb(filter.getToDate()));
        params.addValue("states", JdbcUtil.toDb(filter.getStates()));

        String sql = "select com_id, res_id, voucher, from_date, to_date, updated_at, state, price, currency, paid_deposit, note, contact_person " +
                "from integration.reservation r " +
                "where true " +
                (filter.getComIds() != null ? "and r.com_id in (:comIds) " : "") +
                (filter.getFromDate() != null ? "and (r.from_date > :fromDate or r.to_date > :fromDate) " : "") +
                (filter.getToDate() != null ? "and (r.from_date < :toDate or r.to_date < :toDate) " : "") +
                (filter.getStates() != null ? "and r.state in (:states) " : "");

        return jdbcTemplate.query(sql, params, reservationMapper());
    }

    private RowMapper<ReservationDao> reservationMapper() {
        return (rs, i) -> new ReservationDao(
                rs.getString(COM_ID),
                rs.getString(RES_ID),
                rs.getString(VOUCHER),
                JdbcUtil.getInstant(FROM_DATE, rs),
                JdbcUtil.getInstant(TO_DATE, rs),
                JdbcUtil.getInstant(UPDATED_AT, rs),
                ReservationState.fromDb(rs.getString(STATE)),
                rs.getBigDecimal(PRICE),
                rs.getString(CURRENCY),
                rs.getBigDecimal(PAID_DEPOSIT),
                false,
                rs.getString(NOTE),
                rs.getString(CONTACT_PERSON)
        );
    }

    public void update(@NotNull UpdateReservation obj) {

        if (obj == null)
            throw new RuntimeException("obj cannot be null");

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COM_ID, JdbcUtil.toDb(obj.getComId()));
        params.addValue(RES_ID, JdbcUtil.toDb(obj.getResId()));
        params.addValue(VOUCHER, JdbcUtil.toDb(obj.getVoucher()));
        params.addValue(FROM_DATE, JdbcUtil.toDb(obj.getFromDate()));
        params.addValue(TO_DATE, JdbcUtil.toDb(obj.getToDate()));
        params.addValue(UPDATED_AT, JdbcUtil.toDb(obj.getUpdateAt()));
        params.addValue(STATE, JdbcUtil.toDb(obj.getState()));
        params.addValue(PRICE, JdbcUtil.toDb(obj.getPrice()));
        params.addValue(PAID_DEPOSIT, JdbcUtil.toDb(obj.getPaidDeposit()));
        params.addValue(CONTACT_PERSON, JdbcUtil.toDb(obj.getContactPerson()));

        String sql = "update integration.reservation " +
                "set voucher = coalesce (:voucher, voucher)" +
                ",res_id = coalesce (:res_id, res_id)" +
                ",from_date = coalesce (:from_date, from_date)" +
                ",to_date = coalesce (:to_date, to_date)" +
                ",updated_at = coalesce (:updated_at, updated_at)" +
                ",state = coalesce (:state, state)" +
                ",price = coalesce (:price, price)" +
                ",contact_person = coalesce (:contact_person, contact_person)" +
                ",paid_deposit = coalesce (:paid_deposit, paid_deposit)" +
                "where com_id = :com_id";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
    }

    public void updateByResIdAndVoucher(UpdateReservation obj) {
        if (obj == null)
            throw new RuntimeException("obj cannot be null");

        if (obj.getResId() == null)
            throw new RuntimeException("comId cannot be null");

        if (obj.getVoucher() == null)
            throw new RuntimeException("voucher cannot be null");

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(RES_ID, JdbcUtil.toDb(obj.getResId()));
        params.addValue(VOUCHER, JdbcUtil.toDb(obj.getVoucher()));
        params.addValue(FROM_DATE, JdbcUtil.toDb(obj.getFromDate()));
        params.addValue(TO_DATE, JdbcUtil.toDb(obj.getToDate()));
        params.addValue(UPDATED_AT, JdbcUtil.toDb(obj.getUpdateAt()));
        params.addValue(STATE, JdbcUtil.toDb(obj.getState()));
        params.addValue(COM_ID, JdbcUtil.toDb(obj.getComId()));
        params.addValue(NOTE, JdbcUtil.toDb(obj.getNote()));
        params.addValue(PRICE, JdbcUtil.toDb(obj.getPrice()));


        String sql = "update integration.reservation " +
                "set voucher = coalesce (:voucher, voucher) " +
                ",from_date = coalesce (:from_date, from_date) " +
                ",to_date = coalesce (:to_date, to_date) " +
                ",updated_at = coalesce (:updated_at, updated_at) " +
                ",state = coalesce (:state, state) " +
                ",price = coalesce (:price, price) " +
                ",note = coalesce (:note, note) " +
                "where res_id = :res_id " +
                "and voucher = :voucher";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
    }

    public void delete(String comId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COM_ID, JdbcUtil.toDb(comId));

        String sql = "delete from integration.reservation " +
                "where com_id = :com_id";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
    }

    public List<ReservationForListing> getReservationsForListing(ReservationFilter filter) {

        if (filter == null)
            throw new RuntimeException("filter cannot be null");

        if (filter.getComIds() != null && filter.getComIds().isEmpty())
            throw new RuntimeException("no reservations asking?");

        if (filter.getStates() != null && filter.getStates().isEmpty())
            return Collections.emptyList();

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("comIds", JdbcUtil.toDb(filter.getComIds()));
        params.addValue("fromDate", JdbcUtil.toDb(filter.getFromDate()));
        params.addValue("toDate", JdbcUtil.toDb(filter.getToDate()));
        params.addValue("states", JdbcUtil.toDb(filter.getStates()));

        String sql = "select voucher, res_id, from_date, to_date, state, sum(price) as price, currency, paid_deposit, note, contact_person, (select sum(price) from integration.payment as p where p.voucher = r.voucher) as paid " +
                "from integration.reservation r " +
                "where true " +
                (filter.getComIds() != null ? "and r.com_id in (:comIds) " : "") +
                (filter.getFromDate() != null ? "and (r.from_date > :fromDate or r.to_date > :fromDate) " : "") +
                (filter.getToDate() != null ? "and (r.from_date < :toDate or r.to_date < :toDate) " : "") +
                (filter.getStates() != null ? "and r.state in (:states) " : "") +
                "group by r.voucher, r.from_date, r.to_date, r.state, r.currency, r.res_id, r.paid_deposit, r.note, r.contact_person " +
                "order by r.to_date desc";

        return jdbcTemplate.query(sql, params, reservationForListingMapper());
    }

    private RowMapper<ReservationForListing> reservationForListingMapper() {
        return (rs, i) -> new ReservationForListing(
                rs.getString(RES_ID),
                rs.getString(VOUCHER),
                JdbcUtil.getInstant(FROM_DATE, rs),
                JdbcUtil.getInstant(TO_DATE, rs),
                ReservationState.fromDb(rs.getString(STATE)),
                rs.getBigDecimal("paid"),
                rs.getBigDecimal(PRICE),
                rs.getString(CURRENCY),
                rs.getBigDecimal(PAID_DEPOSIT),
                rs.getString(NOTE),
                rs.getString(CONTACT_PERSON)
        );
    }

}
