package pavel.hruby.previo.integration.repository.septim;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;

@AllArgsConstructor
@Getter
public class SeptimPayment {

    private final long id;
    private final String popis; // voucher
    private final BigDecimal cenas;
    private final Instant datumcas;
    private final String recept;
    private final String pokladna;
    private final Integer smazan; // ano == -1, ne == 0

}
