package pavel.hruby.previo.integration.repository.reservation;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;

@Builder
@Getter
public class UpdateReservation {

    private String resId;
    private String comId;
    private BigDecimal price;
    private String voucher;
    private Instant fromDate;
    private Instant toDate;
    private Instant updateAt;
    private ReservationState state;
    private BigDecimal paidDeposit;
    private String note;
    private String contactPerson;
}
