package pavel.hruby.previo.integration.repository.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.util.JdbcUtil;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class SchedulerRepository {


    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CREATED_AT = "created_at";
    private static final String FINISHED_AT = "finished_at";
    private static final String STATUS = "status";
    private static final String ERROR = "error";


    public void store(SchedulerDao scheduler) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(ID, JdbcUtil.toDb(scheduler.getId()));
        params.addValue(NAME, JdbcUtil.toDb(scheduler.getName()));
        params.addValue(CREATED_AT, JdbcUtil.toDb(scheduler.getCreatedAt()));
        params.addValue(FINISHED_AT, JdbcUtil.toDb(scheduler.getFinishedAt()));
        params.addValue(STATUS, JdbcUtil.toDb(scheduler.getStatus()));
        params.addValue(ERROR, JdbcUtil.toDb(scheduler.getError()));

        String sql = "insert into integration.scheduler (id, name, created_at, finished_at, status, error) " +
                "values (:id, :name, :created_at, :finished_at, :status, :error)";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
        
    }

    public void update(UUID schedulerId, SchedulerStatus status, Instant finishedAt, String errorMessage) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(ID, JdbcUtil.toDb(schedulerId));
        params.addValue(STATUS, JdbcUtil.toDb(status));
        params.addValue(FINISHED_AT, JdbcUtil.toDb(finishedAt));
        params.addValue(ERROR, JdbcUtil.toDb(errorMessage));

        String sql = "update integration.scheduler " +
                "set status = coalesce (:status, status)" +
                ",finished_at = coalesce (:finished_at, finished_at)" +
                ",error = coalesce (:error, error)" +
                "where id = :id";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
    }

    public List<SchedulerDao> getLastSynchronizations(int limit) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", JdbcUtil.toDb(limit));

        String sql = "select id, name, created_at, finished_at, status, error " +
                "from integration.scheduler " +
                "order by created_at desc " +
                "limit :limit";

        return jdbcTemplate.query(sql, params, (rs, i) -> {
              return new SchedulerDao(
                      UUID.fromString(rs.getString(ID)),
                      rs.getString(NAME),
                      JdbcUtil.getInstant(FINISHED_AT, rs),
                      JdbcUtil.getInstant(CREATED_AT, rs),
                      SchedulerStatus.fromDb(rs.getString(STATUS)),
                      rs.getString(ERROR)
              );
        });
    }

    public @Nullable SchedulerDao getLastSynchronizationNoSkipped() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("skipped", JdbcUtil.toDb(SchedulerStatus.SKIPPED));

        String sql = "select id, name, created_at, finished_at, status, error " +
                "from integration.scheduler " +
                "where status <> :skipped " +
                "order by created_at desc " +
                "limit 1";

        List<SchedulerDao> list =  jdbcTemplate.query(sql, params, (rs, i) -> {
            return new SchedulerDao(
                    UUID.fromString(rs.getString(ID)),
                    rs.getString(NAME),
                    JdbcUtil.getInstant(FINISHED_AT, rs),
                    JdbcUtil.getInstant(CREATED_AT, rs),
                    SchedulerStatus.fromDb(rs.getString(STATUS)),
                    rs.getString(ERROR)
            );
        });

        if (list.size() == 0) {
            return null;
        }
        if (list.size() > 1) {
            throw new RuntimeException("Too much syncs found, there should be just one.");
        }
        return list.get(0);
    }
}
