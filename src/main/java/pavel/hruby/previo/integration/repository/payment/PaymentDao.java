package pavel.hruby.previo.integration.repository.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;

@AllArgsConstructor
@Getter
public class PaymentDao {
    private final Long id;
    private final String voucher;
    private final BigDecimal price;
    private final Instant dateTime;
    private final String receipt;
    private final String cashRegister;
    private final boolean cancelled;
}
