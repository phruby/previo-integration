package pavel.hruby.previo.integration.repository.payment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.exception.NoRecordUpdated;
import pavel.hruby.previo.integration.service.payment.UpdatePayment;
import pavel.hruby.previo.integration.util.JdbcUtil;

import java.time.Instant;
import java.util.List;

@Service
public class PaymentRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String ID = "id";
    private static final String VOUCHER = "voucher";
    private static final String PRICE = "price";
    private static final String DATE_TIME = "date_time";
    private static final String RECEIPT = "receipt";
    private static final String CASH_REGISTER = "cash_register";
    private static final String CANCELLED = "cancelled";

    public void storeOrUpdateCancelled(PaymentDao payment) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ID, JdbcUtil.toDb(payment.getId()));
        params.addValue(VOUCHER, JdbcUtil.toDb(payment.getVoucher()));
        params.addValue(PRICE, JdbcUtil.toDb(payment.getPrice()));
        params.addValue(DATE_TIME, JdbcUtil.toDb(payment.getDateTime()));
        params.addValue(RECEIPT, JdbcUtil.toDb(payment.getReceipt()));
        params.addValue(CASH_REGISTER, JdbcUtil.toDb(payment.getCashRegister()));
        params.addValue(CANCELLED, JdbcUtil.toDb(payment.isCancelled()));

        // if payment already exists I do update just cancelled flag based on current value in septim
        String sql = "insert into integration.payment (id, voucher, price, date_time, receipt, cash_register, cancelled) " +
                "values (:id, :voucher, :price, :date_time, :receipt, :cash_register, :cancelled) " +
                "ON CONFLICT (id) DO " +
                "UPDATE SET cancelled = :cancelled";

        JdbcUtil.checkUpdate(jdbcTemplate.update(sql, params));
    }

    public List<PaymentDao> getPaymentsByVoucher(String voucher) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(VOUCHER, JdbcUtil.toDb(voucher));

        String sql = "select id, voucher, price, date_time, receipt, cash_register, cancelled  " +
                "from integration.payment " +
                "where voucher = :voucher " +
                "and not cancelled";

        return jdbcTemplate.query(sql, params, paymentMapper());
    }

    private RowMapper<PaymentDao> paymentMapper() {
        return (rs, i) -> new PaymentDao(
                rs.getLong(ID),
                rs.getString(VOUCHER),
                rs.getBigDecimal(PRICE),
                JdbcUtil.getInstant(DATE_TIME, rs),
                rs.getString(RECEIPT),
                rs.getString(CASH_REGISTER),
                rs.getBoolean(CANCELLED)
        );
    }

    public void unPairPayment(Long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ID, JdbcUtil.toDb(id));

        String sql = "update integration.payment set voucher = null where id = :id";

        if (jdbcTemplate.update(sql, params) != 1) {
            throw new NoRecordUpdated();
        }
    }

    public List<PaymentDao> getUnPairedPayments(Instant fromDate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("fromDate", JdbcUtil.toDb(fromDate));

        String sql = "select id, voucher, price, date_time, receipt, cash_register, cancelled " +
                "from integration.payment p " +
                "where not (select exists(select 1 from integration.reservation as r where p.voucher = r.voucher )) " +
                (fromDate != null ? "and (p.date_time > :fromDate) " : "") +
                "and not cancelled " +
                "order by p.date_time desc";

        return jdbcTemplate.query(sql, params, paymentMapper());
    }

    public void updatePayment(UpdatePayment payment) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ID, JdbcUtil.toDb(payment.getId()));
        params.addValue(VOUCHER, JdbcUtil.toDb(payment.getVoucher()));

        String sql = "update integration.payment " +
                "set voucher = coalesce (:voucher, voucher) " +
                "where id = :id";

        if (jdbcTemplate.update(sql, params) != 1) {
            throw new NoRecordUpdated();
        }
    }
}
