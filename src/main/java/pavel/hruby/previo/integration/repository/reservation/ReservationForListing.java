package pavel.hruby.previo.integration.repository.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;

@AllArgsConstructor
@Getter
@ToString
public class ReservationForListing {

    private final String resId;
    private final String voucher;
    private final Instant from;
    private final Instant to;
    private final ReservationState state;
    private final BigDecimal paid;
    private final BigDecimal price;
    private final String currency;
    private final BigDecimal paidDeposit;
    private final String note;
    private final String contactPerson;
}
