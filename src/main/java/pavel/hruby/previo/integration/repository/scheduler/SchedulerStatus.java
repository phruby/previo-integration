package pavel.hruby.previo.integration.repository.scheduler;

import pavel.hruby.previo.integration.exception.ParameterException;
import pavel.hruby.previo.integration.interfejs.Enumerable;

public enum SchedulerStatus implements Enumerable {

    STARTED("started"),
    FINISHED("finished"),
    SKIPPED("skipped"),
    ERROR("error");

    private final String dbValue;

    SchedulerStatus(String value) {
        this.dbValue = value;
    }

    public static SchedulerStatus fromDb(String value) {
        for (SchedulerStatus type: SchedulerStatus.values() ) {
            if (type.dbValue.equals(value)) {
                return type;
            }
        }
        return null;
    }
    public static SchedulerStatus fromApi(String value) {
        for (SchedulerStatus type: SchedulerStatus.values() ) {
            if (type.dbValue.equals(value)) {
                return type;
            }
        }
        throw new ParameterException("wrong value: " + value);
    }

    public static SchedulerStatus fromApiOptional(String fieldName, String value) {
        if (value == null)
            return null;

        try {
            return fromApi(value);
        } catch (ParameterException ex) {
            throw new ParameterException(fieldName + ": " + ex.getMessage());
        }
    }


    public String getDbValue() {
        return dbValue;
    }
}

