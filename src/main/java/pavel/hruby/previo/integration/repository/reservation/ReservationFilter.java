package pavel.hruby.previo.integration.repository.reservation;


import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.List;

@Builder
@Getter
public class ReservationFilter {

    private List<String> comIds;
    private Instant fromDate;
    private Instant toDate;
    private List<ReservationState> states;

}
