package pavel.hruby.previo.integration.repository.scheduler;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;


@Getter
@AllArgsConstructor
public class SchedulerDao {

    private final UUID id;
    private final String name;
    private final Instant finishedAt;
    private final Instant createdAt;
    private final SchedulerStatus status;
    private final String error;

    public boolean shouldStartNewScheduler() {

        if (status == SchedulerStatus.SKIPPED) {
            throw new RuntimeException("scheduler with status skipped should not be used for evaluation of whether to start new scheduler." + "Make sure you omit this value when retrieving from db");
        }

        return status == SchedulerStatus.FINISHED
                || status == SchedulerStatus.ERROR
                // started before more than twenty minutes
                || (status == SchedulerStatus.STARTED && createdAt.isBefore(Instant.now().minusSeconds(20 * 60)));
    }
}
