package pavel.hruby.previo.integration.repository.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioReservationResponse;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

@AllArgsConstructor
@Getter
@ToString
public class ReservationDao {

    private static final Logger logger = LogManager.getLogger(ReservationDao.class);

    private final String comId;
    private final String resId;
    private final String voucher;
    private final Instant from;
    private final Instant to;
    private final Instant updatedAt;
    private final ReservationState state;
    private final BigDecimal price;
    private final String currency;
    private final BigDecimal paidDeposit;
    private final boolean cancelled;
    private final String note;
    private final String contactPerson;

    public static ReservationDao from (PrevioReservationResponse response, BigDecimal paidDeposit) {
        return new ReservationDao(
                response.getComId(),
                response.getResId(),
                response.getVoucher(),
                TimeUtil.toInstantDateOnly(response.getTerm().getFrom()),
                TimeUtil.toInstantDateOnly(response.getTerm().getTo()),
                null, // ignore update when getting from previo
                null, // ignore state when getting from previo
                new BigDecimal(response.getPrice()),
                response.getCurrency() != null ? response.getCurrency().getCode() : null,
                paidDeposit,
                response.getCancellationDate() != null,
                null,
                response.getContactPerson() != null ? response.getContactPerson().getName() : null
        );
    }

    public static ReservationDao from (PrevioReservationResponse response) {
        return new ReservationDao(
                response.getComId(),
                response.getResId(),
                response.getVoucher(),
                TimeUtil.toInstantDateOnly(response.getTerm().getFrom()),
                TimeUtil.toInstantDateOnly(response.getTerm().getTo()),
                null, // ignore update when getting from previo
                null, // ignore state when getting from previo
                new BigDecimal(response.getPrice()),
                response.getCurrency() != null ? response.getCurrency().getCode() : null,
                null,
                response.getCancellationDate() != null,
                null,
                response.getContactPerson() != null ? response.getContactPerson().getName() : null
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationDao that = (ReservationDao) o;
        return Objects.equals(comId, that.comId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comId);
    }

    public boolean allFieldsEquals(ReservationDao o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (!Objects.equals(comId, o.comId))
            throw new RuntimeException("comId should be same");

        if (!Objects.equals(voucher, o.voucher))
            logger.info("voucher " + comId + " is different." + "New value: " + o.voucher + ", DbValue: " + voucher);

        if (!Objects.equals(from, o.from))
            logger.info("from " + comId + " is different." + "New value: " + o.from + ", DbValue: " + from);

        if (!Objects.equals(to, o.to))
            logger.info("to " + comId + " is different." + "New value: " + o.to + ", DbValue: " + to);

        if (!Objects.equals(resId, o.resId))
            logger.info("resId " + resId + " is different." + "New value: " + o.resId + ", DbValue: " + resId);

        if (!Objects.equals(contactPerson, o.contactPerson))
            logger.info("contactPerson " + contactPerson + " is different." + "New value: " + o.contactPerson + ", DbValue: " + contactPerson);

        if (!Objects.equals(paidDeposit, o.paidDeposit))
            logger.info("paidDeposit " + paidDeposit + " is different." + "New value: " + o.paidDeposit + ", DbValue: " + paidDeposit);


        return Objects.equals(comId, o.comId)
                && Objects.equals(voucher, o.voucher)
                && Objects.equals(from, o.from)
                && Objects.equals(price, o.price)
                && Objects.equals(to, o.to)
                && Objects.equals(contactPerson, o.contactPerson)
                && Objects.equals(resId, o.resId);
    }


}
