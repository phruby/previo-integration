package pavel.hruby.previo.integration.service.sync;

import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.repository.scheduler.SchedulerDao;
import pavel.hruby.previo.integration.repository.scheduler.SchedulerRepository;
import pavel.hruby.previo.integration.repository.scheduler.SchedulerStatus;
import pavel.hruby.previo.integration.service.payment.PaymentSyncLogicService;
import pavel.hruby.previo.integration.service.reservation.ReservationSyncLogicService;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class SynchronizationService {

    private final SchedulerRepository schedulerRepository;
    private final ReservationSyncLogicService reservationSyncLogicService;
    private final PaymentSyncLogicService paymentSyncLogicService;

    private final Logger log = LogManager.getLogger(SynchronizationService.class.getName());

    public void refreshDataFromPrevio(boolean manual) {

        SchedulerDao lastSync = schedulerRepository.getLastSynchronizationNoSkipped();
        // check if running
        if (lastSync == null || lastSync.shouldStartNewScheduler()) {
            UUID schedulerId = null;
            try {
                schedulerId = storeNewScheduler(false, manual);
                log.info("schedulerId started: " + schedulerId);

               // do the work
                reservationSyncLogicService.doTheWork();
                paymentSyncLogicService.doTheWork();

                // mark as finished
                markSchedulerAsFinished(schedulerId);
                log.info("schedulerId finished: " + schedulerId);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                markSchedulerAsError(schedulerId, e);
            }
        } else {
            // previous synchronization still running no longer than twenty minutes
            storeNewScheduler(true, manual);
        }
    }

    private void markSchedulerAsFinished(UUID schedulerId) {
        SchedulerStatus status = SchedulerStatus.FINISHED;
        Instant finishedAt = Instant.now();

        schedulerRepository.update(schedulerId, status, finishedAt, null);
    }

    private void markSchedulerAsError(UUID schedulerId, Exception e) {
        if (schedulerId != null) {

            String stackTrace = Arrays.toString(e.getStackTrace());
            String errorMessage = e.getMessage() + "schedulerId: " + schedulerId + ", stackTrace: " + stackTrace;
            SchedulerStatus status = SchedulerStatus.ERROR;
            Instant finishedAt = Instant.now();
            schedulerRepository.update(schedulerId, status, finishedAt, errorMessage);
        } else {
            throw new RuntimeException("there is no id of scheduler, should never happen");
        }
    }

    private UUID storeNewScheduler(boolean skipped, boolean manual) {
        SchedulerDao scheduler = newScheduler(skipped, manual);
        schedulerRepository.store(scheduler);
        return scheduler.getId();
    }

    private SchedulerDao newScheduler(boolean skipped, boolean manual) {
       return new SchedulerDao(
                UUID.randomUUID(),
                manual ? "manual sync": "automatic sync",
                null,
                Instant.now(),
                skipped ? SchedulerStatus.SKIPPED : SchedulerStatus.STARTED,
                skipped ? "Predchozi synchronizace zacala pred mene nez 20 minutami a stale bezi" : null
        );
    }

    public List<SchedulerDao> getLastSynchronizations(int limit) {
        return schedulerRepository.getLastSynchronizations(limit);
    }
}
