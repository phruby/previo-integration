package pavel.hruby.previo.integration.service.payment;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.config.SeptimDatabaseConfig;
import pavel.hruby.previo.integration.repository.payment.PaymentDao;
import pavel.hruby.previo.integration.repository.septim.SeptimPayment;
import pavel.hruby.previo.integration.repository.septim.SeptimRepository;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

@Service
@AllArgsConstructor
public class PaymentSyncLogicService {

    private final SeptimRepository septimRepository;
    private final PaymentService paymentService;
    private final SeptimDatabaseConfig septimDatabaseConfig;

    public void doTheWork() {
        if (septimDatabaseConfig.isEnabled()) {
            refreshNewDataFromSeptim();
        }
    }

    private void refreshNewDataFromSeptim() {
//        // Data from db are stored as sequence
        List<SeptimPayment> septimPayments = septimRepository.getPayments();

        List<PaymentDao> paymentsToInsert = septimPayments.stream().map(mapPayments()).collect(toList());

        paymentService.storeOrUpdate(paymentsToInsert);
    }

    private Function<SeptimPayment, PaymentDao> mapPayments() {
        return item -> new PaymentDao(
                item.getId(),
                item.getPopis(),
                item.getCenas(),
                item.getDatumcas(),
                item.getRecept(),
                item.getPokladna(),
                item.getSmazan() == -1
        );
    }

}
