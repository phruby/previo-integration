package pavel.hruby.previo.integration.service.payment;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UpdatePayment {

    private long id;
    private String voucher;
}
