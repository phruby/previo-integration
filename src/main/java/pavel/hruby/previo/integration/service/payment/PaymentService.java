package pavel.hruby.previo.integration.service.payment;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.repository.payment.PaymentDao;
import pavel.hruby.previo.integration.repository.payment.PaymentRepository;

import java.time.Instant;
import java.util.List;

@Service
@AllArgsConstructor
public class PaymentService {

    private final PaymentRepository paymentRepository;

    void storeOrUpdate(List<PaymentDao> payments) {
        for (PaymentDao payment : payments) {
            paymentRepository.storeOrUpdateCancelled(payment);
        }
    }

    public List<PaymentDao> getPaymentsByVoucher(String voucher) {
        if (voucher == null) {
              throw new RuntimeException("voucher cannot be null");
        }

        return paymentRepository.getPaymentsByVoucher(voucher);
    }

    public void unPairPayment(Long id) {
        paymentRepository.unPairPayment(id);
    }

    public List<PaymentDao> getUnPairedPayments(Instant fromDate) {
        return paymentRepository.getUnPairedPayments(fromDate);
    }

    public void updatePayment(UpdatePayment updatePayment) {
        paymentRepository.updatePayment(updatePayment);
    }
}
