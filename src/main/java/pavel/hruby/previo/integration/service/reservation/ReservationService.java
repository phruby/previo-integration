package pavel.hruby.previo.integration.service.reservation;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.http.previo.rest.PaymentResponse;
import pavel.hruby.previo.integration.http.previo.rest.RestPrevioHttpService;
import pavel.hruby.previo.integration.http.previo.xml.XmlPrevioHttpService;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioPaymentResponseItem;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioReservationResponse;
import pavel.hruby.previo.integration.repository.reservation.ReservationDao;
import pavel.hruby.previo.integration.repository.reservation.ReservationFilter;
import pavel.hruby.previo.integration.repository.reservation.ReservationForListing;
import pavel.hruby.previo.integration.repository.reservation.ReservationRepository;
import pavel.hruby.previo.integration.repository.reservation.UpdateReservation;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.*;

@Service
@AllArgsConstructor
public class ReservationService {

    private final XmlPrevioHttpService xmlPrevioHttpService;
    private final RestPrevioHttpService restPrevioHttpService;
    private final ReservationRepository reservationRepository;

    void store(ReservationDao reservation) {
        reservationRepository.store(reservation);
    }

    void delete(ReservationDao reservation) {
        reservationRepository.delete(reservation.getComId());
    }

    public void update(UpdateReservation update) {
        reservationRepository.update(update);
    }

    public void updateByResIdAndVoucher(UpdateReservation update) {
        reservationRepository.updateByResIdAndVoucher(update);
    }

    public List<ReservationForListing> getReservationsForListing(ReservationFilter filter) {
        return reservationRepository.getReservationsForListing(filter);
    }

    List<ReservationDao> getReservationsForLast180daysFromPrevio() {

        LocalDate from = TimeUtil.getDaysAgo(180);
        LocalDate to = TimeUtil.getDaysInFuture(180);
        List<PrevioReservationResponse> reservations = xmlPrevioHttpService.getReservations(from, to, null);
        PaymentResponse[] payments = restPrevioHttpService.getPayments(from);

        if (reservations == null) {
            throw new RuntimeException("should not happen, no reservations for last 180 days");
        }

        if (payments == null) {
            throw new RuntimeException("should not happen, no payments for last 180 days");
        }

        List<ReservationDao> responseList = new ArrayList<>();
        for (PrevioReservationResponse reservation : reservations) {

            BigDecimal paidDeposit = findDepositBy(reservation.getComId(), payments);
            responseList.add(ReservationDao.from(reservation, paidDeposit));
        }

        return responseList;
    }

    List<ReservationDao> getReservationsFromDatabaseByComIds(List<String> comIds) {

        ReservationFilter filter = ReservationFilter.builder()
                .comIds(comIds)
                .build();

        return reservationRepository.getReservations(filter);
    }

    List<ReservationDao> getReservationsForLast30daysFromDb() {
        Instant fromDate = TimeUtil.toInstant(TimeUtil.getDaysAgo(30));

        ReservationFilter filter = ReservationFilter.builder().fromDate(fromDate).build();
        return reservationRepository.getReservations(filter);
    }

    List<ReservationDao> getReservationsFromPrevioByComIds(List<String> comIds) {
        List<PrevioReservationResponse> reservations = xmlPrevioHttpService.getReservations(TimeUtil.getDaysAgo(60), TimeUtil.getDaysInFuture(60), comIds);

        if (reservations == null) {
            throw new RuntimeException("should not happen, no reservations for last 60 days");
        } else {
            return reservations.stream().map(ReservationDao::from).collect(toList());
        }
    }

    // todo delete -> not used anymore, findDepositBy is used now
    BigDecimal getPaidDepositForPayment(String comId) {
        List<PrevioPaymentResponseItem> payments = xmlPrevioHttpService.getPayments(comId);
        if (payments == null) {
            return BigDecimal.ZERO;
        }

        return payments.stream()
                .map(PrevioPaymentResponseItem::getPriceSum)
                .map(BigDecimal::new)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal findDepositBy(String comId, PaymentResponse[] payments) {
        return Arrays.stream(payments)
                .filter(item -> item.getRoomReservationIds().contains(comId))
                .map(PaymentResponse::getPriceSum)
                .findFirst().orElse(BigDecimal.ZERO);
    }
}
