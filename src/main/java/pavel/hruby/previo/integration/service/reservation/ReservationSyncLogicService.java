package pavel.hruby.previo.integration.service.reservation;


import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.config.PrevioConfig;
import pavel.hruby.previo.integration.repository.reservation.ReservationDao;
import pavel.hruby.previo.integration.repository.reservation.UpdateReservation;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ReservationSyncLogicService {

    private final ReservationService reservationService;
    private final PrevioConfig previoConfig;

    private final Logger log = LogManager.getLogger(ReservationSyncLogicService.class.getName());

    public void doTheWork() {
        if (previoConfig.isEnabled()) {
            newRefreshDataFromPrevio();
            clearDataFromDatabase();
        }
    }

    private void clearDataFromDatabase() {
        List<ReservationDao> dbReservations = reservationService.getReservationsForLast30daysFromDb();
        List<ReservationDao> previoReservations = reservationService.getReservationsFromPrevioByComIds(idsOf(dbReservations));

        for (ReservationDao reservationInDatabase : dbReservations) {

            int index = previoReservations.indexOf(reservationInDatabase);
            // exist information in previo
            if (index == -1 || previoReservations.get(index).isCancelled()) {
                // not found or cancelled -> delete in database
                log.info("reservation not found in previo, deleting " + reservationInDatabase);

                reservationService.delete(reservationInDatabase);
            }
        }

    }

//    private void refreshDataFromPrevio() {
//        // data from previo
//        List<ReservationDao> previoReservations = reservationService.getReservationsForLast180daysFromPrevio();
//        List<ReservationDao> dbReservations = reservationService.getReservationsFromDatabaseByComIds(idsOf(previoReservations));
//
//        for (ReservationDao previoReservation : previoReservations) {
//
//            // exist in database?
//            int index = dbReservations.indexOf(previoReservation);
//            if (index == -1) { // not found in database
//
//                // if not exist -> store in db
//                if (!previoReservation.isCancelled()) {
//                    reservationService.store(previoReservation);
//                    log.info("stored in db " + previoReservation);
//                }
//            } else { // found in database
//
//                // if reservation is cancelled delete in db
//                if (previoReservation.isCancelled()) {
//
//                    reservationService.delete(previoReservation);
//                    log.info("deleted in db " + previoReservation);
//                } else {
//                    // update reservation if any change
//                    ReservationDao dbReservation = dbReservations.get(index);
//
//                    // if some value is changed -> update db
//                    if (anyChange(previoReservation, dbReservation)) {
//
//                        UpdateReservation update = UpdateReservation.builder()
//                                .price(previoReservation.getPrice())
//                                .resId(previoReservation.getResId())
//                                .comId(previoReservation.getComId())
//                                .voucher(previoReservation.getVoucher())
//                                .fromDate(previoReservation.getFrom())
//                                .toDate(previoReservation.getTo())
//                                .updateAt(Instant.now())
//                                .paidDeposit(previoReservation.getPaidDeposit())
//                                .contactPerson(previoReservation.getContactPerson())
//                                .build();
//
//                        reservationService.update(update);
//                        log.info("updating in db " + previoReservation);
//                    }
//                }
//            }
//        }
//    }

    private void newRefreshDataFromPrevio() {
        // data from previo
        List<ReservationDao> previoReservations = reservationService.getReservationsForLast180daysFromPrevio();
        List<ReservationDao> dbReservations = reservationService.getReservationsFromDatabaseByComIds(idsOf(previoReservations));

        for (ReservationDao previoReservation : previoReservations) {

            // exist in database?
            int index = dbReservations.indexOf(previoReservation);
            if (index == -1) { // not found in database

                // if not exist -> store in db
                if (!previoReservation.isCancelled()) {
                    reservationService.store(previoReservation);
                    log.info("stored in db " + previoReservation);
                }
            } else { // found in database

                // if reservation is cancelled delete in db
                if (previoReservation.isCancelled()) {

                    reservationService.delete(previoReservation);
                    log.info("deleted in db " + previoReservation);
                } else {
                    // update reservation if any change
                    ReservationDao dbReservation = dbReservations.get(index);

                    // if some value is changed -> update db
                    if (anyChange(previoReservation, dbReservation)) {

                        UpdateReservation update = UpdateReservation.builder()
                                .price(previoReservation.getPrice())
                                .resId(previoReservation.getResId())
                                .comId(previoReservation.getComId())
                                .voucher(previoReservation.getVoucher())
                                .fromDate(previoReservation.getFrom())
                                .toDate(previoReservation.getTo())
                                .updateAt(Instant.now())
                                .paidDeposit(previoReservation.getPaidDeposit())
                                .contactPerson(previoReservation.getContactPerson())
                                .build();

                        reservationService.update(update);
                        log.info("updating in db " + previoReservation);
                    }
                }
            }
        }
    }

    private boolean anyChange(ReservationDao reservationInPrevio, ReservationDao reservationInDb) {
        return !reservationInDb.allFieldsEquals(reservationInPrevio);
    }

    private List<String> idsOf(List<ReservationDao> reservationsFromPrevio) {
        return reservationsFromPrevio.stream().map(ReservationDao::getComId).collect(toList());
    }
}
