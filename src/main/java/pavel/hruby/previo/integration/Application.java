package pavel.hruby.previo.integration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import pavel.hruby.previo.integration.config.InstanceConfig;

import javax.annotation.PostConstruct;

@SpringBootApplication
@RestController
public class Application {

    @Autowired
    private InstanceConfig instanceConfig;

    private final Logger log = LogManager.getLogger(Application.class.getName());

	@PostConstruct
    private void logInstance() {
        log.info("running instance " + instanceConfig.getName());
    }

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}