package pavel.hruby.previo.integration.http.previo.xml.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class PrevioPaymentResponseItem {

    @XmlElement(name = "priceSum")
    private String priceSum;
}
