package pavel.hruby.previo.integration.http.previo.xml.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class PrevioReservationResponse {

    @XmlElement(name = "comId")
    private String comId;

    @XmlElement(name = "resId")
    private String resId;

    @XmlElement(name = "lanId")
    private String lanId;

    @XmlElement(name = "voucher")
    private String voucher;

    @XmlElement(name = "term")
    private Term term;

    @XmlElement(name = "contactPerson")
    private ContactPerson contactPerson;

    @XmlElement(name = "price")
    private String price;

    @XmlElement(name = "currency")
    private Currency currency;

    @XmlElement(name = "cancellationDate")
    private String cancellationDate;
}
