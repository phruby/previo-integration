package pavel.hruby.previo.integration.http.previo.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pavel.hruby.previo.integration.config.PrevioConfig;
import pavel.hruby.previo.integration.http.previo.xml.request.ComIds;
import pavel.hruby.previo.integration.http.previo.xml.request.PaymentRequest;
import pavel.hruby.previo.integration.http.previo.xml.request.ReservationsRequest;
import pavel.hruby.previo.integration.http.previo.xml.request.Term;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioPaymentResponse;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioPaymentResponseItem;
import pavel.hruby.previo.integration.http.previo.xml.response.PrevioReservationResponse;
import pavel.hruby.previo.integration.http.previo.xml.response.ReservationsResponse;

import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;


@Service
public class XmlPrevioHttpService {

    private static URL SEARCH_RESERVATION_URL;
    private static URL GET_DOCUMENTS_URL;

    private final Logger log = LogManager.getLogger(XmlPrevioHttpService.class.getName());

    static {
        try {
            SEARCH_RESERVATION_URL = new URL("https://api.previo.app/x1/hotel/searchReservations");
            GET_DOCUMENTS_URL = new URL("https://api.previo.app/x1/hotel/getDocuments");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    private PrevioConfig previoConfig;

    public List<PrevioReservationResponse> getReservations(@NotNull LocalDate from, @NotNull LocalDate to, List<String> comIds) {
        if (from == null)
            throw new RuntimeException("from cannot be null");

        if (to == null)
            throw new RuntimeException("to cannot be null");

        ReservationsRequest body = createGetReservationsBody(from, to, comIds);

        try {
            HttpURLConnection connection = createConnection(SEARCH_RESERVATION_URL);

            OutputStream os = connection.getOutputStream();

            // process xml
            JAXBContext requestContext = JAXBContext.newInstance(ReservationsRequest.class);
            JAXBElement<ReservationsRequest> element = new JAXBElement<>(new QName("", "request"), ReservationsRequest.class, null, body);
            Marshaller marshaller = requestContext.createMarshaller();
            marshaller.marshal(element, os);
//            marshaller.marshal(element, System.out);

            os.flush();
            int responseCode = connection.getResponseCode();
            if(responseCode != 200) {
                log.error("Wrong response code: " + responseCode);
            }
            String result = readResult(connection);

            JAXBContext responseContext = JAXBContext.newInstance(ReservationsResponse.class);
            Unmarshaller unmarshaller = responseContext.createUnmarshaller();

            StringReader reader = new StringReader(result);
            ReservationsResponse response = (ReservationsResponse) unmarshaller.unmarshal(reader);
            return response.getReservation();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public List<PrevioPaymentResponseItem> getPayments(String comId) {

        PaymentRequest body = createGetPaymentRequest(comId);

        try {
            HttpURLConnection connection = createConnection(GET_DOCUMENTS_URL);

            OutputStream os = connection.getOutputStream();

            // process xml
            JAXBContext requestContext = JAXBContext.newInstance(PaymentRequest.class);
            JAXBElement<PaymentRequest> element = new JAXBElement<>(new QName("", "request"), PaymentRequest.class, null, body);

            Marshaller marshaller = requestContext.createMarshaller();
            marshaller.marshal(element, os);
//            marshaller.marshal(element, System.out);

            os.flush();
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                log.error("Wrong response code: " + responseCode);
            }
            String result = readResult(connection);

            JAXBContext responseContext = JAXBContext.newInstance(PrevioPaymentResponse.class);
            Unmarshaller unmarshaller = responseContext.createUnmarshaller();

            StringReader reader = new StringReader(result);
            PrevioPaymentResponse response = (PrevioPaymentResponse) unmarshaller.unmarshal(reader);
            if (response == null)
                return null;

            if (response.getDocuments() == null)
                return null;

            return response.getDocuments();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private ReservationsRequest createGetReservationsBody(LocalDate from, LocalDate to, List<String> ids) {

        boolean comIdsProvided = ids != null;

        ComIds comIds = null;
        if(comIdsProvided) {
            comIds = new ComIds(ids);
        }

        return new ReservationsRequest(
                    previoConfig.getLogin(),
                    previoConfig.getPassword(),
                    previoConfig.getHotId(),
                    comIds,
                    new Term(
                            from.toString(),
                            to.toString()
                    )
        );
    }

    private PaymentRequest createGetPaymentRequest(String comId) {

        return new PaymentRequest(
                previoConfig.getLogin(),
                previoConfig.getPassword(),
                previoConfig.getHotId(),
                comId
        );
    }

    private HttpURLConnection createConnection(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/xml");
        return connection;
    }

    private static String readResult(HttpURLConnection con) throws IOException {

        // do we have error stream available?
        InputStream resultInputStream = con.getErrorStream();
        if(resultInputStream == null) {

            // no errors, use standard stream
            resultInputStream = con.getInputStream();
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(resultInputStream, StandardCharsets.UTF_8));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();
        return response.toString();
    }

}
