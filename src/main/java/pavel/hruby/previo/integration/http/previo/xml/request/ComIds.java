package pavel.hruby.previo.integration.http.previo.xml.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class ComIds {

    @XmlElement
    private List<String> comId;
}


