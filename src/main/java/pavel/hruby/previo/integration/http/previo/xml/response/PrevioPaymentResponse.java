package pavel.hruby.previo.integration.http.previo.xml.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@XmlRootElement(name = "documents")
public class PrevioPaymentResponse {

    @XmlElement(name = "document")
    private List<PrevioPaymentResponseItem> documents;
}
