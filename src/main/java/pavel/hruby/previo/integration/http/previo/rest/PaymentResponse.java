package pavel.hruby.previo.integration.http.previo.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
public class PaymentResponse {
    private List<String> roomReservationIds; // comIds
    private BigDecimal priceSum; // paidDeposit
}
