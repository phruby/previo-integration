package pavel.hruby.previo.integration.http.previo.xml.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@XmlRootElement(name = "request")
@XmlType(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentRequest {

    @XmlElement(name = "login")
    private String login;
    @XmlElement(name = "password")
    private String password;
    @XmlElement(name = "hotId")
    private String hotId;
    @XmlElement(name = "comId")
    private String comId;
}
