package pavel.hruby.previo.integration.http.previo.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pavel.hruby.previo.integration.config.PrevioConfig;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.time.LocalDate;
import java.util.Base64;

@Service
public class RestPrevioHttpService {

    private final Logger log = LogManager.getLogger(RestPrevioHttpService.class.getName());

    @Autowired
    private PrevioConfig previoConfig;

    public PaymentResponse[] getPayments(LocalDate from) {
        try {

            String apiUrl = "https://api.previo.cz/rest/billing/documents";

            String uri = UriComponentsBuilder.fromHttpUrl(apiUrl)
                    .queryParam("filterDateType", "paidDate")
                    .queryParam("filterFrom", from)
                    .toUriString();

            ResponseEntity<PaymentResponse[]> responseEntity = new RestTemplate(withTimeout()).exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers()),
                    PaymentResponse[].class
            );

            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                log.error("invalid status code: " + responseEntity.getStatusCode() + ", responseEntity: " + responseEntity);
                throw new RuntimeException("invalid status code: " + responseEntity.getStatusCode().toString());
            }


            return responseEntity.getBody();
        } catch (Exception e) {
            log.error("error during calling for documents", e);
            throw new RuntimeException("error during calling for documents: " + e.getMessage(), e);
        }
    }

    private HttpHeaders headers () {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Previo-Hotel-Id", previoConfig.getHotId());
        headers.set("X-Previo-Language-ID", "2");

        String userNamePwd = previoConfig.getLogin() + ":" + previoConfig.getPassword();
        String base64 = new String(Base64.getEncoder().encode(userNamePwd.getBytes()));
        headers.set("Authorization", "Basic " + base64);

        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    private ClientHttpRequestFactory withTimeout() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();

        // Set the read timeout (time for waiting for data) and connect timeout (time for establishing a connection)
        factory.setConnectTimeout(10_000);
        factory.setReadTimeout(20_000);

        return factory;
    }

}
