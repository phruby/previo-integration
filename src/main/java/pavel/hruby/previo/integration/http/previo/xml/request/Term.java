package pavel.hruby.previo.integration.http.previo.xml.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Term {

    @XmlElement
    private String from;
    @XmlElement
    private String to;
}
