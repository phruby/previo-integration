package pavel.hruby.previo.integration.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix="septim.database")
public class SeptimDatabaseConfig {
    private boolean enabled;
    private String url;
    private String username;
    private String password;
    private String aswUser;
    private String aswPassword;
}
