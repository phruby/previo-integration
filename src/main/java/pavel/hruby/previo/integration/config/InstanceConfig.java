package pavel.hruby.previo.integration.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix="instance")
public class InstanceConfig {


    private String name;
}
