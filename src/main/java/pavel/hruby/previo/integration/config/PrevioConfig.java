package pavel.hruby.previo.integration.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix="previo.credentials")
public class PrevioConfig {


    private boolean enabled;
    private String login;
    private String password;
    private String hotId;
    private String old;


}
