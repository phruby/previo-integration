package pavel.hruby.previo.integration.api.reservation;


import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pavel.hruby.previo.integration.api.reservation.request.GetReservationsRequest;
import pavel.hruby.previo.integration.api.reservation.request.UpdateReservationRequest;
import pavel.hruby.previo.integration.api.reservation.response.GetReservationResponse;
import pavel.hruby.previo.integration.api.reservation.response.ReservationResponse;
import pavel.hruby.previo.integration.exception.ParameterException;
import pavel.hruby.previo.integration.repository.reservation.ReservationFilter;
import pavel.hruby.previo.integration.repository.reservation.ReservationState;
import pavel.hruby.previo.integration.repository.reservation.UpdateReservation;
import pavel.hruby.previo.integration.repository.scheduler.SchedulerDao;
import pavel.hruby.previo.integration.repository.scheduler.SchedulerStatus;
import pavel.hruby.previo.integration.service.reservation.ReservationService;
import pavel.hruby.previo.integration.service.sync.SynchronizationService;
import pavel.hruby.previo.integration.util.ListHelper;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static pavel.hruby.previo.integration.util.TimeUtil.getDaysAgo;
import static pavel.hruby.previo.integration.util.TimeUtil.toInstant;


@RestController
@AllArgsConstructor
public class ReservationApi {

    private final ReservationService reservationService;
    private final SynchronizationService synchronizationService;

    private final Logger log = LogManager.getLogger(ReservationApi.class.getName());

    @PostMapping("/backend/reservations-get")
    GetReservationResponse getReservations(@RequestBody GetReservationsRequest request) {

        log.info("/backend/reservations-get called request:" + request);

        List<ReservationState> states = ListHelper.map("states", request.getStates(), ReservationState::fromApi);

        Instant fromDate = request.getDaysAgo() != null ? toInstant(getDaysAgo(request.getDaysAgo())) : null;

        ReservationFilter filter = ReservationFilter.builder()
                .states(states)
                .fromDate(fromDate)
                .toDate(TimeUtil.tomorrow(false))
                .build();

        List<ReservationResponse> reservations = reservationService.getReservationsForListing(filter).stream()
                .map(ReservationResponse::mapReservation)
                .collect(Collectors.toList());

        return new GetReservationResponse(reservations);
    }

    @PostMapping("/backend/reservation-update")
    void updateReservation(@RequestBody UpdateReservationRequest request) {

        log.info("/backend/reservation-update called request:" + request);
        if (request.getResId() == null) {
            throw new ParameterException("resId field is missing");
        }

        if (request.getVoucher() == null) {
            throw new ParameterException("voucher field is missing");
        }

        ReservationState newState = ReservationState.fromApiOptional("state", request.getState());

        UpdateReservation update = UpdateReservation.builder()
                .resId(request.getResId())
                .voucher(request.getVoucher())
                .state(newState)
                .note(request.getNote())
                .build();

        reservationService.updateByResIdAndVoucher(update);
    }

    private boolean lastSyncBefore10minutes() {

        List<SchedulerDao> lastSync = synchronizationService.getLastSynchronizations(1);
        if (lastSync.size() != 1)
            throw new RuntimeException("no synchronization found)");

        if (lastSync.get(0).getFinishedAt() == null && lastSync.get(0).getStatus() == SchedulerStatus.STARTED)
            return false; // scheduler started but is not finished yet

        if (lastSync.get(0).getStatus() == SchedulerStatus.ERROR)
            throw new ParameterException("last sync was Error. Data are not up to date");

        // was the work done before 10 minutes
        return lastSync.get(0).getFinishedAt().isBefore(Instant.now().minusSeconds(10 * 60));
    }
}
