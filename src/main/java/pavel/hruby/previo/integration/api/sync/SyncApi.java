package pavel.hruby.previo.integration.api.sync;


import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pavel.hruby.previo.integration.service.sync.SynchronizationService;

import java.util.List;

import static java.util.stream.Collectors.toList;


@RestController
@AllArgsConstructor
public class SyncApi {

    private final Logger log = LogManager.getLogger(SyncApi.class.getName());

    private final SynchronizationService synchronizationService;

    @PostMapping("/backend/sync-data")
    void syncDate() {
        log.info("/backend/sync-data");
        synchronizationService.refreshDataFromPrevio(true);
    }

    @PostMapping("/backend/get-last-synchronization")
    GetSynchronizationResponse getLastSynchronizations() {
        log.info("/backend/get-last-synchronization called");
        List<SynchronizationResponse> list = synchronizationService.getLastSynchronizations(10).stream().map(SynchronizationResponse::from).collect(toList());

        return new GetSynchronizationResponse(list);
    }
}
