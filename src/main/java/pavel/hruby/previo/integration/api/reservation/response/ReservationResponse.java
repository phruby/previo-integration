package pavel.hruby.previo.integration.api.reservation.response;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import pavel.hruby.previo.integration.repository.reservation.ReservationForListing;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class ReservationResponse {

    @ApiModelProperty
    private final String resId;
    @ApiModelProperty
    private final String voucher;
    @ApiModelProperty(notes = "format yyyy-MM-dd")
    private final String from;
    @ApiModelProperty(notes = "format yyyy-MM-dd")
    private final String to;
    @ApiModelProperty(notes = "ReservationState-values: [new, ok]")
    private final String state;
    @ApiModelProperty(notes = "already paid")
    private final BigDecimal paid;
    @ApiModelProperty(notes = "should be paid")
    private final BigDecimal price;
    @ApiModelProperty(notes = "paid deposit")
    private final BigDecimal paidDeposit;
    @ApiModelProperty(notes = "should be paid currency")
    private final String currency;
    @ApiModelProperty(notes = "note")
    private final String note;
    @ApiModelProperty(notes = "contact person")
    private final String contactPerson;

    public static ReservationResponse mapReservation(ReservationForListing item) {
        return new ReservationResponse(
                item.getResId(),
                item.getVoucher(),
                TimeUtil.usePattern(item.getFrom(), "dd.MM.yyyy"),
                TimeUtil.usePattern(item.getTo(), "dd.MM.yyyy"),
                item.getState().getDbValue(),
                item.getPaid(),
                item.getPrice(),
                item.getPaidDeposit(),
                item.getCurrency(),
                item.getNote(),
                item.getContactPerson()
        );
    }

}
