package pavel.hruby.previo.integration.api.reservation.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GetReservationsRequest {

    @ApiModelProperty("ReservationState-values: [new, ok]")
    private List<String> states;

    @ApiModelProperty("number of days in Past to Search")
    private Integer daysAgo;
}
