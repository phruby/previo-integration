package pavel.hruby.previo.integration.api.payment.request;


import lombok.Data;

@Data
public class UnPairPaymentRequest {

    private Long id;

}
