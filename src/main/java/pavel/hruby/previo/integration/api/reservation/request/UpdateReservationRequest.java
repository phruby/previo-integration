package pavel.hruby.previo.integration.api.reservation.request;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdateReservationRequest {

    private String resId;
    private String voucher;
    @ApiModelProperty("ReservationState-values: [new, ok]")
    private String state;
    private String note;
}
