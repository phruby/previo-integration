package pavel.hruby.previo.integration.api.payment.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;


@AllArgsConstructor
@Getter
public class GetPaymentsResponse {

    private final List<PaymentResponse> payments;

}
