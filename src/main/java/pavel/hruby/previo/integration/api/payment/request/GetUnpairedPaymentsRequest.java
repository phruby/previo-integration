package pavel.hruby.previo.integration.api.payment.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetUnpairedPaymentsRequest {

    @ApiModelProperty("number of days in Past to Search")
    private Integer daysAgo;
}
