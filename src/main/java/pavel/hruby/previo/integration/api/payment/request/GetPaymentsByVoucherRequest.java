package pavel.hruby.previo.integration.api.payment.request;


import lombok.Data;

@Data
public class GetPaymentsByVoucherRequest {

    String voucher;

}
