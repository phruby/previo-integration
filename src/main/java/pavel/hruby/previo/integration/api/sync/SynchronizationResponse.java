package pavel.hruby.previo.integration.api.sync;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import pavel.hruby.previo.integration.repository.scheduler.SchedulerDao;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.util.UUID;


@Getter
@AllArgsConstructor
public class SynchronizationResponse {

    private final UUID id;
    private final String name;
    private final String finishedAt;
    private final String createdAt;
    @ApiModelProperty(notes = "[started, finished, skipped, error]")
    private final String status;
    private final String error;

    public static SynchronizationResponse from(SchedulerDao item) {
        return new SynchronizationResponse(
                item.getId(),
                item.getName(),
                TimeUtil.usePattern(item.getFinishedAt(), "dd.MM.yyyy HH:mm:ss"),
                TimeUtil.usePattern(item.getCreatedAt(), "dd.MM.yyyy HH:mm:ss"),
                item.getStatus().getDbValue(),
                item.getError()
        );
    }
}
