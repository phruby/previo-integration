package pavel.hruby.previo.integration.api.payment.request;


import lombok.Data;

@Data
public class PairPaymentRequest {

    private Long id;
    private String voucher;

}
