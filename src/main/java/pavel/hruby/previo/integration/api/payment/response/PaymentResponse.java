package pavel.hruby.previo.integration.api.payment.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import pavel.hruby.previo.integration.repository.payment.PaymentDao;
import pavel.hruby.previo.integration.util.TimeUtil;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class PaymentResponse {

    private final Long id;

    @ApiModelProperty(notes = "format yyyy-MM-dd")
    private final String dateTime;
    private final String receipt;
    private final BigDecimal price;
    private final String cashRegister;
    private final String voucher;

    public static PaymentResponse from(PaymentDao paymentDao) {
        return new PaymentResponse(
                paymentDao.getId(),
                TimeUtil.usePattern(paymentDao.getDateTime(), "dd.MM.yyyy"),
                paymentDao.getReceipt(),
                paymentDao.getPrice(),
                paymentDao.getCashRegister(),
                paymentDao.getVoucher()
        );
    }
}
