package pavel.hruby.previo.integration.api.sync;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class GetSynchronizationResponse {

    List<SynchronizationResponse> synchronizations;
}
