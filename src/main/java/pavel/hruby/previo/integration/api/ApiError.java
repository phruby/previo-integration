package pavel.hruby.previo.integration.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiError {

    private final String message;
    private final ApiErrorCode code;
}
