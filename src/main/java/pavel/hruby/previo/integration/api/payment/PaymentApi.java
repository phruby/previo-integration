package pavel.hruby.previo.integration.api.payment;


import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pavel.hruby.previo.integration.api.payment.request.GetPaymentsByVoucherRequest;
import pavel.hruby.previo.integration.api.payment.request.GetUnpairedPaymentsRequest;
import pavel.hruby.previo.integration.api.payment.request.PairPaymentRequest;
import pavel.hruby.previo.integration.api.payment.request.UnPairPaymentRequest;
import pavel.hruby.previo.integration.api.payment.response.GetPaymentsResponse;
import pavel.hruby.previo.integration.api.payment.response.PaymentResponse;
import pavel.hruby.previo.integration.exception.ParameterException;
import pavel.hruby.previo.integration.repository.payment.PaymentDao;
import pavel.hruby.previo.integration.repository.reservation.ReservationDao;
import pavel.hruby.previo.integration.service.payment.PaymentService;
import pavel.hruby.previo.integration.service.payment.UpdatePayment;
import pavel.hruby.previo.integration.service.reservation.ReservationService;

import java.time.Instant;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static pavel.hruby.previo.integration.util.TimeUtil.getDaysAgo;
import static pavel.hruby.previo.integration.util.TimeUtil.toInstant;


@RestController
@AllArgsConstructor
public class PaymentApi {

    private final PaymentService paymentService;
    private final ReservationService reservationService;

    private final Logger log = LogManager.getLogger(PaymentApi.class.getName());

    @PostMapping("/backend/get-payments-by-voucher")
    GetPaymentsResponse getReservations(@RequestBody GetPaymentsByVoucherRequest request) {

        log.info("/backend/get-payments-by-voucher called: request " + request);

        if (request.getVoucher() == null) {
            throw new ParameterException("voucher cannot be null");
        }

        List<PaymentDao> payments = paymentService.getPaymentsByVoucher(request.getVoucher());

        List<PaymentResponse> paymentsResponse = payments.stream()
                .map(PaymentResponse::from)
                .collect(toList());

        return new GetPaymentsResponse(paymentsResponse);
    }

    @PostMapping("/backend/get-un-paired-payments")
    GetPaymentsResponse getUnPairedPayments(@RequestBody GetUnpairedPaymentsRequest request) {

        log.info("/backend/get-un-paired-payments called");
        Instant fromDate = request.getDaysAgo() != null ? toInstant(getDaysAgo(request.getDaysAgo())) : null;

        List<PaymentDao> payments = paymentService.getUnPairedPayments(fromDate);

        List<PaymentResponse> paymentsResponse = payments.stream().map(PaymentResponse::from).collect(toList());

        return new GetPaymentsResponse(paymentsResponse);
    }

    @PostMapping("/backend/pair-payment")
    void pairPayment(@RequestBody PairPaymentRequest request) {

        log.info("/backend/pair-payment called request: " + request);
        if (request.getId() == null) throw new ParameterException("id cannot be null");

        if (request.getVoucher() == null) throw new ParameterException("voucher cannot be null");

        UpdatePayment updatePayment = UpdatePayment.builder().id(request.getId()).voucher(request.getVoucher()).build();

        paymentService.updatePayment(updatePayment);
    }

    @PostMapping("/backend/un-pair-payment")
    void unPairPayment(@RequestBody UnPairPaymentRequest request) {

        log.info("/backend/un-pair-payment called: request " + request);
        if (request.getId() == null) throw new ParameterException("id cannot be null");

        paymentService.unPairPayment(request.getId());
    }
}
