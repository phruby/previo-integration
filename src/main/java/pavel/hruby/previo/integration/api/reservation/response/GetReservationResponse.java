package pavel.hruby.previo.integration.api.reservation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@Getter
@ToString
public class GetReservationResponse {

    private final List<ReservationResponse> reservations;

}
