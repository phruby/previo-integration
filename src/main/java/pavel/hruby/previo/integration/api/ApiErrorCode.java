package pavel.hruby.previo.integration.api;

public enum ApiErrorCode {


    NO_RECORD,
    INVALID_REQUEST_BODY;

}
