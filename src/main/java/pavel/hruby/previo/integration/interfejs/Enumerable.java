package pavel.hruby.previo.integration.interfejs;

public interface Enumerable {

    String getDbValue();
}
