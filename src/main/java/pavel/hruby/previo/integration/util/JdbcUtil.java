package pavel.hruby.previo.integration.util;

import pavel.hruby.previo.integration.exception.NoRecordUpdated;
import pavel.hruby.previo.integration.interfejs.Enumerable;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.*;

public class JdbcUtil {

    public static Timestamp toDb(Instant instant) {
        if (instant != null) {
            return Timestamp.from(instant.truncatedTo(ChronoUnit.MICROS));
        }
        return null;
    }

    public static Timestamp todDbDate(Instant instant) {
        if (instant != null) {
        LocalDate localDate = TimeUtil.toLocaleDate(instant);
        Instant instantDateOnly = TimeUtil.toInstant(localDate);
            return Timestamp.from(instantDateOnly.truncatedTo(ChronoUnit.MICROS));
        }
        return null;
    }

    public static String toDb(Enumerable enumerable) {
        if (enumerable != null) {
            return enumerable.getDbValue();
        }
        return null;
    }

    public static Boolean toDb(Boolean bool) {
        return bool;
    }

    public static long toDb(long value) {
        return value;
    }

    public static Integer toDb(Integer value) {
        return value;
    }

    public static <T> List<String> toDb(List<T> values) {
        if (values == null)
            return null;

        if (values.isEmpty())
            return Collections.emptyList();

        Object firstElement = values.get(0);

        if (firstElement instanceof String)
            return values.stream().map(item -> (String) item).collect(toList());

        if (firstElement instanceof Enumerable)
            return values.stream().map(item -> ((Enumerable) item).getDbValue()).collect(toList());


        throw new RuntimeException("not supported type for list to db");
    }

    public static Instant getInstant(String fieldName, ResultSet rs) throws SQLException {
        Timestamp timestamp = rs.getTimestamp(fieldName);
        if (timestamp == null) {
            return null;
        }
        return timestamp.toInstant().truncatedTo(ChronoUnit.MICROS);
    }

    public static String toDb(String value) {
        return value;
    }

    public static boolean toDb(boolean value) {
        return value;
    }

    public static BigDecimal toDb(BigDecimal value) {
        return value;
    }

    public static UUID toDb(UUID value) {
        return value;
    }

    public static void checkUpdate(int value) {
        if (value == 0) {
            throw new NoRecordUpdated();
        }
    }

    public static <T> T getOneItem(List<T> list) {
        if(list.size() == 1) {
            return list.get(0);
        }
        throw new RuntimeException("wrong count in db, should be 1 but is " + list.size());
    }
}
