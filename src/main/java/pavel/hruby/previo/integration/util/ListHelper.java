package pavel.hruby.previo.integration.util;

import pavel.hruby.previo.integration.exception.ParameterException;
import pavel.hruby.previo.integration.interfejs.Enumerable;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListHelper {


    public static <T> List<T> map(String fieldName, List<String> values, Function<String, T> typeConstructor) {

        if (values == null)
            return null;

        try {

            return values.stream()
                    .map(typeConstructor)
                    .peek(Objects::requireNonNull)
                    .collect(Collectors.toList());

        } catch (ParameterException ex) {
            throw new ParameterException(fieldName + ": " + ex.getMessage());
        }

    }
}
