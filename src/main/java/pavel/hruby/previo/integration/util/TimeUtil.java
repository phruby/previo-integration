package pavel.hruby.previo.integration.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TimeUtil {

    private static final String PREVIO_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static Instant toInstantDateOnly(String value) {

        LocalDateTime localDateTime = LocalDateTime.parse(value, DateTimeFormatter.ofPattern(PREVIO_DATE_PATTERN));
        Instant instant =  localDateTime.atZone(getPragueZoneId()).toInstant();
        if (instant != null) {
            LocalDate localDate = TimeUtil.toLocaleDate(instant);
            return toInstant(localDate);
        }
        return null;
    }

    private static ZoneId getPragueZoneId() {
        return ZoneId.of("Europe/Prague");
    }

    public static Instant toInstant(LocalDate localDate) {
        return localDate.atStartOfDay(getPragueZoneId()).toInstant();
    }

    public static LocalDate getDaysAgo (int days) {
        return LocalDate.now().minusDays(days);
    }

    public static LocalDate getDaysInFuture (int days) {
        return LocalDate.now().plusDays(days);
    }

    public static LocalDate toLocaleDate(Instant time) {
        if (time == null){
            return null;
        }
        return time.atZone(getPragueZoneId()).toLocalDate();
    }

    public static LocalDateTime toLocaleDateTime(Instant instant) {
        if(instant == null) {
            return null;
        }
        return instant.atZone(getPragueZoneId()).toLocalDateTime();
    }

    public static Instant tomorrow(boolean endOfDay) {
        if (endOfDay)
            return LocalDate.now().plusDays(2).atStartOfDay(getPragueZoneId()).toInstant().minusMillis(1);

        return LocalDate.now().plusDays(1).atStartOfDay(getPragueZoneId()).toInstant();
    }

    public static String usePattern(Instant instant, String pattern) {
        if (instant == null)
            return null;

        LocalDateTime localDatetime = toLocaleDateTime(instant);
        return localDatetime.format(DateTimeFormatter.ofPattern(pattern));
    }
}
