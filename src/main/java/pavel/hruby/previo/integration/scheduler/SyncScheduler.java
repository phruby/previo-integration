package pavel.hruby.previo.integration.scheduler;

import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pavel.hruby.previo.integration.service.sync.SynchronizationService;

@EnableScheduling
@Component
@AllArgsConstructor
public class SyncScheduler {

    private final SynchronizationService synchronizationService;

    private final Logger log = LogManager.getLogger(SyncScheduler.class.getName());

    // runs at 6am and 2pm
    @Scheduled(cron = "0 0 6,14 * * ?")
    public void runMigration() {
        log.info("automatic synchronization started");
        synchronizationService.refreshDataFromPrevio(false);
    }

}
