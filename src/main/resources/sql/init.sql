create table if not exists integration.reservation
(
    com_id     varchar primary key,
    res_id     varchar,
    voucher    varchar not null,
    from_date  timestamp,
    to_date    timestamp,
    updated_at timestamp,
    state      varchar,
    price      numeric,
    currency   varchar
);

create index if not exists reservatopm_voucher_index
    on integration.reservation (voucher);

create table if not exists integration.scheduler
(
    id          uuid primary key,
    name        varchar,
    created_at  timestamp,
    finished_at timestamp,
    status      varchar,
    error       varchar
);

create table if not exists integration.payment
(
    id numeric primary key not null,
    voucher varchar,
    price numeric,
    date_time timestamp,
    receipt varchar,
    cash_register varchar
);

create index if not exists paymemt_voucher_index
    on integration.payment (voucher);

alter table integration.reservation add column if not exists paid_deposit numeric default 0;
alter table integration.reservation add column if not exists note varchar;
alter table integration.reservation add column if not exists contact_person varchar;


alter table integration.payment add column if not exists cancelled boolean default false;

create index if not exists paymemt_date_time_index
    on integration.payment (date_time);

